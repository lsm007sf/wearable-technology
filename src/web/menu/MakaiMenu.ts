import { showLogin, showSetDisplayName, showSetEmail, showSetAvatar } from '../control/userControl';
import { Menu } from '../Menu';

export class MakaiMenu extends Menu {
  public constructor(parent: Menu) {
    super('Makai 评论系统管理', parent);
    this.addItem('Makai 令牌', {
      small: true,
      button: true,
    }).onClick(() => {
      showLogin();
    });
    this.addItem('修改名字', {
      small: true,
      button: true,
    }).onClick(() => {
      showSetDisplayName();
    });
    this.addItem('修改邮箱', {
      small: true,
      button: true,
    }).onClick(() => {
      showSetEmail();
    });
    this.addItem('修改头像', {
      small: true,
      button: true,
    }).onClick(() => {
      showSetAvatar();
    });
  }
}
